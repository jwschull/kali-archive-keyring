-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

This file is signed with Kali's key. Its purpose is
to ensure that we can check its signature with the
key provided in kali-archive-keyring (thus ensuring
that it has not expired yet).
-----BEGIN PGP SIGNATURE-----

iQIzBAEBCAAdFiEERMZROo5Ps9MIdfdY7URP8H2NC/YFAlppv84ACgkQ7URP8H2N
C/aanBAAn7hgkXslTwllHf2tV7m32I1jvPSrM0BpnI/eduZ4VPo7XRgo+CVhRBnj
v2PBw/Tob739nzpwdpGoRCZLLuQYV52HoVvmPBMv42PXl1XruO6/xXAZvC5k36Ui
5zM9fmyTQCH1dk6ObwR0vq6kCkvfeQvHVfK8olBTK1enO9lTKoHmkBrP/zhFOQOw
EtcCxTWqUDSnC3nc+p8fUfhCSCpB1yib3AHYPix7UScA5egK0/qD9eotVk5n2A28
32uP2jLYLHhXVqzk/Tg4hAilTsDjEvD7QzVT+Ybn9KUWsC+skRoL++D1iOEhnFfP
EXKIC9TaohO5eNgdxGW8shFGu6Z7NyTAj2A9UQqSQKy2qJSYA38E+8/Nq70rSdpL
YSoxoZG9fbZhuAoytJ1+qKxzAeaRNIM/Uwb5tWGp5+jv3Ka+D4Tv3xdL4PBowrVK
FSVWtEQ95uZG+4NsTADHjlnbwFJuaHDmumau5WfnzwnoFla1J3QHMQOSYAdABZX8
VjKAzpWP3ZxwbUTBE28436oMXBHgSaInfea4sOHHLl0xkuXGOQYyLJBifLEg0LQQ
Svn68iX2CmRd5hK4G1xYfURdkK0zq7WLL/VtiElK1Lz7oinrPGn4LeVm4+z+4Ycw
Hp2MqA56KeL04wfNiyy+bTkAdof782w2P4IXFtdZ9tA8fFLInGQ=
=2mr1
-----END PGP SIGNATURE-----
