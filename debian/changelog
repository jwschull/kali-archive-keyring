kali-archive-keyring (2022.1) kali-dev; urgency=medium

  [ Kali Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ Ben Wilson ]
  * Update email address

  [ Arnaud Rebillout ]
  * Redo the package without jetring
  * Bump Standards-Version and debhelper compat
  * Update key (expiry date set to 2025-01-24)

 -- Arnaud Rebillout <arnaudr@kali.org>  Tue, 25 Jan 2022 21:39:54 +0700

kali-archive-keyring (2020.2) kali-dev; urgency=medium

  * Fix build by adding missing --no-keyring

 -- Raphaël Hertzog <raphael@offensive-security.com>  Fri, 17 Jan 2020 21:45:59 +0100

kali-archive-keyring (2020.1) kali-dev; urgency=medium

  * Configure git-buildpackage for Kali
  * Update URL in GitLab's CI configuration file
  * Update expiration date of the archive key
  * Strip trust packets from generated keyrings

 -- Raphaël Hertzog <raphael@offensive-security.com>  Fri, 17 Jan 2020 19:15:25 +0100

kali-archive-keyring (2018.2) kali-dev; urgency=medium

  * Update Maintainer field
  * Add GitLab's CI configuration file
  * Drop dependencies on gpgv and gnupg
  * Add Vcs-* and Rules-Require-Root fields

 -- Raphaël Hertzog <raphael@offensive-security.com>  Wed, 24 Jul 2019 17:09:53 +0200

kali-archive-keyring (2018.1) kali-dev; urgency=medium

  * Update expiration date of Kali's main key.
  * Update debhelper compat level.
  * Add autopkgtests to ensure the key will not expire in the next year.

 -- Raphaël Hertzog <buxy@kali.org>  Thu, 25 Jan 2018 12:17:49 +0100

kali-archive-keyring (2015.2) kali; urgency=medium

  * Update expiration date of Kali's main key.

 -- Raphaël Hertzog <buxy@kali.org>  Tue, 03 Feb 2015 16:24:14 +0100

kali-archive-keyring (2013.1) kali; urgency=low

  * Introduce kali-archive-keyring-udeb so that net-retriever can depend on
    it. This will ensure that we have the required keys on the initrd.
  * Synchronize dependencies with those of debian-archive-keyring 2012.4.

 -- Raphaël Hertzog <buxy@kali.org>  Fri, 18 Jan 2013 14:11:04 +0100

kali-archive-keyring (2012.03.07) kali; urgency=low

  * Initial release.

 -- Raphaël Hertzog <buxy@kali.org>  Wed, 07 Mar 2012 15:22:27 +0100
